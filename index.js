'use strict';

let app = require('express')();
let http = require('http').createServer(app);
let io = require('socket.io')(http);
let moment = require('moment');
moment().format();

let mongoose = require('mongoose');
mongoose.connect('mongodb://mongo:27017/anonymous-chat', { useNewUrlParser: true });

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

let connectionsLoggerSchema = new mongoose.Schema({
    access_token: String,
    timestamp: String,
    action: String
});

let messagesSchema = new mongoose.Schema({
    access_token: String,
    timestamp: String,
    message: String
});

let connectionLogger = mongoose.model('ConnectionLogger', connectionsLoggerSchema);
let message = mongoose.model('Message', messagesSchema);



app.get('/', function(req, res) {
    // res.send('<h1>Hello world</h1>');
    res.sendFile(__dirname + '/index.html');
});

io
    .use(function(socket, next) {
        if (socket.handshake.query && socket.handshake.query.access_token) {
            console.log('User with token: ' + socket.handshake.query.access_token + ' connected :)');

            let model = new connectionLogger({ access_token: socket.handshake.query.access_token, timestamp: moment().toLocaleString(), action: 'connected' });

            model.save(function(err, result) {
                if (err) return console.error(err);
            });

            next();
        }
    })
    .on('connection', function(socket) {
        socket.on('disconnect', function() {
            console.log('User with token: ' + socket.handshake.query.access_token + ' disconected :(');

            let model = new connectionLogger({ access_token: socket.handshake.query.access_token, timestamp: moment().toLocaleString(), action: 'disconnected' });

            model.save(function(err, result) {
                if (err) return console.error(err);
            });

        });

        socket.on('chat message', function(msg) {

            let model = new message({ access_token: socket.handshake.query.access_token, timestamp: moment().toLocaleString(), message: msg.message });

            io.emit('chat message', { access_token: model.access_token, message: model.message, timestamp: model.timestamp });

            console.log('From ' + socket.handshake.query.access_token + ': ' + msg.message);


            model.save(function(err, result) {
                if (err) return console.error(err);
            });

        });

    });

http.listen(3000, function() {
    console.log('Listening on *:3000');
});